import React, { useState } from 'react';
import './App.css';
import data from './test.json'


function App() {
    const [ stateData, setData ] = useState(data);

    const setItem = (index) => {
        const choosenItem = [...stateData]
        choosenItem[index].choosen = !choosenItem[index].choosen
        setData(choosenItem)
    }

    const showChoosenItems = () => {
       let alertMessage = '';
       stateData.forEach((item) => {
         if (item.choosen === true) { alertMessage += ` ${item.id}` }
       })
        alert(alertMessage)
    }

    return (
        <div>
        <div className='wrap'>
            {
                stateData.map((item, index) =>
                    <div
                        className="wrap_item"
                        style={item.choosen ? {backgroundColor: '#eee'}: null}
                        onClick={() => setItem(index)}
                        key={index}
                    >
                      <p className="wrap_item_title" style={item.choosen ? {color: 'red'}: null}>{item.name}</p>
                      <img src={item.imageUrl} alt="beautiful cat" className='image' />
                    </div>
                )
            }
        </div>
          <button
              className='button'
              onClick={showChoosenItems}
          >
            Submit
          </button>
        </div>

    );
}

export default App;
